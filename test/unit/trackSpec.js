'use strict';
var expect = require('referee').expect,
    track = require('../../lib/track');

describe('track', function () {
    var trackData = {},
        customTrack;

    beforeEach(function () {
        trackData.pieces = [
            {length: 10},
            {length: 20},
            {length: 30},
            {length: 40},
            {length: 50},
            {length: 60},
            {length: 70},
            {length: 80},
            {length: 90}
        ];
        trackData.lanes = [ {
            distanceFromCenter: -20,
            index: 0
        } ];
        customTrack = track(trackData);
    });

    describe('getDistanceToNextCurve', function () {
        it('should calculate the distance to next curve', function () {
            var currentPosition = {
                piecePosition: {
                    pieceIndex: 1,
                    inPieceDistance: 50,
                    lane: { startLaneIndex: 0, endLandeIndex: 0 }
                }
            };

            trackData.pieces = [
                {length: 100},
                {length: 100},
                {length: 50},
                {angle: 45, radius: 100}
            ];
            customTrack = track(trackData);

            expect(customTrack.getDistanceToNextCurve(currentPosition)).toEqual(100);
        });

        it('should work even on a lap overflow', function () {
            var currentPosition = {
                piecePosition: {
                    pieceIndex: 3,
                    inPieceDistance: 50,
                    lane: { startLaneIndex: 0, endLandeIndex: 0 }
                }
            };

            trackData.pieces = [
                {length: 100},
                {length: 100},
                {angle: 45, radius: 100},
                {length: 100},
                {length: 50}
            ];
            customTrack = track(trackData);

            expect(customTrack.getDistanceToNextCurve(currentPosition)).toEqual(300);
        });
    });

    describe('segmentation', function () {
        it('should get the segmentized pieces', function () {
            var expectedSegments = [
                { type: 'straight', length: 250, pieceStart: 0, pieceEnd: 2 },
                { type: 'rightTurn', length: 188.49555921538757, angle: 45, radius: 100, pieceStart: 3, pieceEnd: 4 },
                { type: 'rightTurn', length: 86.39379797371932, angle: 22.5, radius: 200, pieceStart: 5, pieceEnd: 5 },
                { type: 'straight', length: 100, pieceStart: 6, pieceEnd: 6 },
                { type: 'leftTurn', length: 125.66370614359172, pieceStart: 7, pieceEnd: 8, angle: -45, radius: 100 },
                { type: 'rightTurn', length: 282.74333882308133, pieceStart: 9, pieceEnd: 11, angle: 45, radius: 100 }
            ];

            trackData.pieces = [
                {length: 100},
                {length: 100},
                {length: 50},
                {angle: 45, radius: 100},
                {angle: 45, radius: 100},
                {angle: 22.5, radius: 200},
                {length: 100},
                {angle: -45, radius: 100},
                {angle: -45, radius: 100},
                {angle: 45, radius: 100},
                {angle: 45, radius: 100},
                {angle: 45, radius: 100}
            ];
            customTrack = track(trackData);

            expect(customTrack.getSegments()).toEqual(expectedSegments);
        });

        it('should combine the first and last segment if they have the same type', function () {
            var expectedSegments = [
                { type: 'straight', length: 450, pieceStart: 5, pieceEnd: 2 },
                { type: 'rightTurn', length: 188.49555921538757, pieceStart: 3, pieceEnd: 4, angle: 45, radius: 100 }
            ];

            trackData.pieces = [
                {length: 100},
                {length: 100},
                {length: 50},
                {angle: 45, radius: 100},
                {angle: 45, radius: 100},
                {length: 100},
                {length: 100}
            ];
            customTrack = track(trackData);

            expect(customTrack.getSegments()).toEqual(expectedSegments);
        });
    });

    describe('piece extraction between two points', function () {
        it('should extract pieces where startIndex is lower than endIndex', function () {
            var expectedPieces = [
                {length: 30},
                {length: 40},
                {length: 50},
                {length: 60}
            ];
            expect(customTrack.getPiecesBetweenTwoPoints(1, 6)).toEqual(expectedPieces);
        });

        it('should extract pieces where startIndex is greater than endIndex', function () {
            var expectedPieces = [
                {length: 80},
                {length: 90},
                {length: 10},
                {length: 20}
            ];
            expect(customTrack.getPiecesBetweenTwoPoints(6, 2)).toEqual(expectedPieces);
        });

        it('should return an empty array if startIndex is equals endIndex', function () {
            var expectedPieces = [];
            expect(customTrack.getPiecesBetweenTwoPoints(2, 2)).toEqual(expectedPieces);
        });

        it('should return an empty array if there is no piece between startIndex and endIndex', function () {
            var expectedPieces = [];
            expect(customTrack.getPiecesBetweenTwoPoints(2, 3)).toEqual(expectedPieces);
            expect(customTrack.getPiecesBetweenTwoPoints(8, 0)).toEqual(expectedPieces);
        });

    });

    describe('calculate the length of a track piece', function () {
        var curveTrack,
            curveTrackData = {};

        beforeEach(function () {
            curveTrackData.pieces = [
                {length: 100},
                {radius: 100, angle: 45 },
                {radius: 100, angle: -45 }
            ];
            curveTrackData.lanes = [ {
                distanceFromCenter: -20,
                index: 0
            } ];
            curveTrack = track(curveTrackData);
        });

        it('should calculate the correct length of a straight piece', function () {
            expect(curveTrack.getLengthOfPiece(curveTrackData.pieces[0], 0)).toBe(100);
        });

        it('should calculate the correct length of a bend piece', function () {
            expect(curveTrack.getLengthOfPiece(curveTrackData.pieces[1], 0)).toBeNear(94,24777960769379, 0.01);
        });

        it('should calculate the correct length for a left turn', function () {
            expect(curveTrack.getLengthOfPiece(curveTrackData.pieces[2], 0)).toBeNear(62.8318530717959, 0.01);
        });
    });

    describe('calculate the distance between two positions', function () {

        it('get the correct distance with multiple pieces between the positions', function () {
            var startPosition = {
                    piecePosition: {
                        pieceIndex: 0,
                        inPieceDistance: 0.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                },
                endPosition = {
                    piecePosition: {
                        pieceIndex: 4,
                        inPieceDistance: 20.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                };

            expect(customTrack.getDistance(startPosition, endPosition)).toBe(120);
        });

        it('get the correct distance with multiple pieces between the positions with lap overflow', function () {
            var startPosition = {
                    piecePosition: {
                        pieceIndex: 7,
                        inPieceDistance: 0.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                },
                endPosition = {
                    piecePosition: {
                        pieceIndex: 1,
                        inPieceDistance: 20.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                };

            expect(customTrack.getDistance(startPosition, endPosition)).toBe(200);
        });

        it('get the correct distance within a single piece', function () {
            var startPosition = {
                    piecePosition: {
                        pieceIndex: 2,
                        inPieceDistance: 10.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                },
                endPosition = {
                    piecePosition: {
                        pieceIndex: 2,
                        inPieceDistance: 20.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                };

            expect(customTrack.getDistance(startPosition, endPosition)).toBe(10);
        });

        it('get the correct distance without whole pieces between the positions', function () {
            var startPosition = {
                    piecePosition: {
                        pieceIndex: 1,
                        inPieceDistance: 10.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                },
                endPosition = {
                    piecePosition: {
                        pieceIndex: 2,
                        inPieceDistance: 20.0,
                        lane: {
                            startLaneIndex: 0,
                            endLandeIndex: 0
                        }
                    }
                };

            expect(customTrack.getDistance(startPosition, endPosition)).toBe(30);
        });
    });
});
