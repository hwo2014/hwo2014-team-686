'use strict';
var referee = require('referee'),
    expect = referee.expect,
    net = require('net'),
    sinon = require('sinon'),
    refereeSinon = require('referee-sinon'),
    createClient = require('../../lib/client'),
    Stream = require('stream');

refereeSinon(referee, sinon);

describe('client', function () {
    var connect, tcpStream;

    beforeEach(function () {
        connect = sinon.stub(net, 'connect');
        tcpStream = new Stream();
        sinon.spy(tcpStream, 'on');
        tcpStream.write = sinon.stub();
        connect.returns(tcpStream);
    });

    afterEach(function () {
        connect.restore();
    });

    it('should establish a connection to the given host and port', function () {
        var expectedOptions = {
            host: 'example.com',
            port: 1337
        };

        createClient('example.com', 1337);

        expect(connect).toHaveBeenCalledWith(expectedOptions);
    });

    describe('sendMessage', function () {
        var client;

        beforeEach(function () {
            client = createClient('example.com', 1337);
            sinon.stub(tcpStream, 'emit');
        });

        it('should emit an error for unknown message types', function () {
            var expectedError = 'can’t send unknown message type "foo"',
                returnValue;

            returnValue = client.sendMessage('foo', null);

            expect(client.emit).toHaveBeenCalledWithMatch('error', { message: expectedError });
            expect(returnValue).toBeFalse();
        });

        it('should emit an event for every outgoing message', function () {
            client.sendMessage('join', 'foo');
            expect(client.emit).toHaveBeenCalledWithExactly('outgoing message', 'join', 'foo');
        });

        it('should send the serialized command in a single line', function () {
            var expectedPayload = {
                    msgType: 'join',
                    data: { foo: 'bar' }
                },
                expectedSerializedPayload = JSON.stringify(expectedPayload) + '\n';

            client.sendMessage('join', { foo: 'bar' });

            expect(tcpStream.write).toHaveBeenCalledWith(expectedSerializedPayload);
        });
    });

    describe('receiving incoming messages', function () {
        var client;

        beforeEach(function () {
            client = createClient('example.com', 1337);
            sinon.spy(tcpStream, 'emit');
        });

        it('should emit an event for unknown message types', function () {
            tcpStream.emit('data', JSON.stringify({msgType: 'foo', data: 'bar'}));
            expect(client.emit).toHaveBeenCalledWithExactly('unknown message type', 'foo', 'bar');
        });

        it('should emit an event for every incoming known message', function () {
            tcpStream.emit('data', JSON.stringify({msgType: 'gameInit', data: 'foo'}));
            expect(client.emit).toHaveBeenCalledWithExactly('incoming message', 'gameInit', 'foo');
        });

        it('should emit an event for the specific message type', function () {
            tcpStream.emit('data', JSON.stringify({msgType: 'gameInit', data: 'foo'}));
            expect(client.emit).toHaveBeenCalledWithExactly('gameInit', 'foo');
        });
    });
});
