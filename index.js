'use strict';

var createClient = require('./lib/client'),
    serverHost = process.argv[2],
    serverPort = process.argv[3],
    botName = process.argv[4],
    botKey = process.argv[5],
    createBot = require('./lib/bot'),
    logger = require('./lib/logger'),
    client;

client = createClient(serverHost, serverPort);
logger.info('Bot ', botName, ' connecting to', serverHost + ':' + serverPort);
createBot(botName, botKey, client);
