'use strict';

var bunyan = require('bunyan');

module.exports = bunyan.createLogger({
    name: 'Team 1UP',
    stream: process.stdout,
    level: 'debug'
});
