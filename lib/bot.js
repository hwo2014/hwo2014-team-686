'use strict';

var logger = require('./logger'),
    track = require('./track'),
    lastPosition,
    currentTrack,
    curveThrottle = 0.654,
    curveSpeed = curveThrottle * 10;

function getStoppingAcceleration(fastSpeed, slowSpeed, distance) {
    return ((fastSpeed * fastSpeed) - (slowSpeed * slowSpeed)) / (2 * distance);
}

function getStoppingDistance(fastSpeed, slowSpeed, stoppingAcceleration) {
    return Math.abs(((fastSpeed * fastSpeed) - (slowSpeed * slowSpeed)) / (2 * stoppingAcceleration));
}

module.exports = function createBot(botName, botKey, client) {
    var v1, v2;

    client.on('connect', function () {
        client.sendMessage('join', {
            name: botName,
            key: botKey
        });
    });

    client.on('incoming message', function (messageType, data) {
        logger.info('Incoming message %s %j', messageType, data);
    });

    client.on('outgoing message', function (messageType, data) {
        logger.info('Outgoing message %s %j', messageType, data);
    });

    client.on('unknown message type', function (messageType, data) {
        logger.warn('Received unknown message %s %j', messageType, data);
    });

    client.on('error', function (error) {
        logger.error(error);
    });

    client.on('carPositions', function (data) {
        var distance = 0,
            currentPosition,
            currentLap,
            currentPieceIndex,
            stoppingAcceleration;

        currentPosition = data[0];
        currentLap = currentPosition.piecePosition.lap;
        currentPieceIndex = currentPosition.piecePosition.pieceIndex;

        if (lastPosition) {
            distance = currentTrack.getDistance(lastPosition, currentPosition);
            logger.info('Current distance per game tick: %d', distance);
        }

        if (currentLap === 0 && currentPieceIndex < 2) {
            if (currentPieceIndex === 0) {
                client.sendMessage('throttle', 1.0);
            } else {
                if (!v1) {
                    v1 = distance;
                }
                client.sendMessage('throttle', 0.0);
            }
        } else {
            if (!v2) {
                v2 = distance;
            }
            stoppingAcceleration = getStoppingAcceleration(v1, v2, 100);

            if (getStoppingDistance(distance, curveSpeed, stoppingAcceleration) >= currentTrack.getDistanceToNextCurve(currentPosition) && distance > curveSpeed) {
                client.sendMessage('throttle', 0.0);
            } else {
                if (currentTrack.isCurve(currentPosition)) {
                    client.sendMessage('throttle', curveThrottle);
                } else {
                    client.sendMessage('throttle', 1.0);
                }
            }
        }

        lastPosition = currentPosition;
    });

    client.on('join', logger.info.bind(logger, 'Joined'));
    client.on('gameStart', logger.info.bind(logger, 'Race started %j'));
    client.on('gameEnd', logger.info.bind(logger, 'Race ended %j'));
    client.on('gameInit', function (data) {
        currentTrack = track(data.race.track);
    });
};
