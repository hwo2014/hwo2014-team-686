'use strict';

var net = require('net'),
    JSONStream = require('JSONStream'),
    knownIncomingMessageTypes = [
        'yourCar',
        'gameInit',
        'gameStart',
        'carPositions',
        'gameEnd',
        'tournamentEnd',
        'crash',
        'spawn',
        'lapFinished',
        'dnf',
        'finish',
        'turboAvailable',
        'join'
    ],
    knownOutgoingMessageTypes = [
        'join',
        'throttle',
        'switchLane',
        'turbo',
        'ping',
        'joinRace',
        'createRace'
    ];

function connect(server, port) {
    var options = {
        port: port,
        host: server
    };

    return net.connect(options);
}

module.exports = function createClient(server, port) {
    var tcpStream = connect(server, port),
        jsonStream = tcpStream.pipe(JSONStream.parse());

    tcpStream.sendMessage = function sendMessage(messageType, data) {
        var payload = {
                msgType: messageType,
                data: data
            },
            serializedPayload = JSON.stringify(payload) + '\n',
            error;

        if (knownOutgoingMessageTypes.indexOf(messageType) === -1) {
            error = new Error('can’t send unknown message type "' + messageType + '"');
            tcpStream.emit('error', error);
            return false;
        }

        tcpStream.emit('outgoing message', messageType, data);
        return tcpStream.write(serializedPayload);
    };

    jsonStream.on('data', function (response) {
        var msgType = response.msgType;

        if (knownIncomingMessageTypes.indexOf(msgType) === -1) {
            tcpStream.emit('unknown message type', msgType, response.data);
        } else {
            tcpStream.emit(msgType, response.data);
            tcpStream.emit('incoming message', msgType, response.data);
        }
    });

    return tcpStream;
};
