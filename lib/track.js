'use strict';

var _ = require('underscore');

module.exports = function (track) {
    var pieces = track.pieces,
        lanes = track.lanes,
        segments;

    function isStraight(piece) {
        return piece.hasOwnProperty('length');
    }

    function getPieceType(piece) {
        if (isStraight(piece)) {
            return 'straight';
        } else if (piece.angle < 0) {
           return 'leftTurn';
        }

        return 'rightTurn';
    }

    function getSegmentProperties(piece) {
        var properties = { type: getPieceType(piece) };

        if (properties.type !== 'straight') {
            properties.angle = piece.angle;
            properties.radius = piece.radius;
        }

        return properties;
    }

    function belongsToSegment(segment, pieceOrSegment) {
        var propertiesToCompare = [ 'type', 'radius', 'angle' ],
            properties = _.pick(segment, propertiesToCompare),
            isSegment = pieceOrSegment.hasOwnProperty('type'),
            segmentProperties;

        if (isSegment) {
            segmentProperties = _.pick(pieceOrSegment, propertiesToCompare);
        } else {
            segmentProperties = getSegmentProperties(pieceOrSegment);
        }

        return _.isEqual(properties, segmentProperties);
    }

    function getLengthOfPiece(piece, laneIndex) {
        var radius,
            isLeftTurn = piece.angle < 0;

        if (piece.hasOwnProperty('length')) {
            return piece.length;
        }

        if (isLeftTurn) {
            radius = piece.radius + lanes[laneIndex].distanceFromCenter;
        } else {
            radius = piece.radius - lanes[laneIndex].distanceFromCenter;
        }

        return Math.PI * radius * Math.abs(piece.angle) / 180;
    }

    function getPiecesBetweenTwoPoints(startIndex, endIndex) {
        var selectedPieces = [];

        if (startIndex === endIndex || startIndex === endIndex - 1) {
            return selectedPieces;
        }

        if (startIndex < endIndex) {
            return pieces.slice(startIndex + 1, endIndex);
        }

        return pieces.slice(startIndex + 1).concat(pieces.slice(0, endIndex));
    }

    function getDistance(startPosition, endPosition) {
        var startPieceIndex = startPosition.piecePosition.pieceIndex,
            endPieceIndex = endPosition.piecePosition.pieceIndex,
            startPiece = pieces[startPieceIndex],
            startInPieceDistance = startPosition.piecePosition.inPieceDistance,
            endInPieceDistance = endPosition.piecePosition.inPieceDistance,
            piecesBetweenStartAndEnd = getPiecesBetweenTwoPoints(startPieceIndex, endPieceIndex),
            distance = 0,
            currentLane = startPosition.piecePosition.lane.startLaneIndex;

        if (startPieceIndex === endPieceIndex) {
            distance += endInPieceDistance - startInPieceDistance;
        } else {
            distance += getLengthOfPiece(startPiece, currentLane) - startInPieceDistance;
            distance += endInPieceDistance;
        }

        distance = piecesBetweenStartAndEnd.reduce(function (currentDistance, piece) {
            return currentDistance + getLengthOfPiece(piece, currentLane);
        }, distance);

        return distance;
    }

    function createSegment(piece, pieceStart) {
        var segment = getSegmentProperties(piece);
        segment.pieceStart = pieceStart;
        segment.length = getLengthOfPiece(piece, 0);

        return segment;
    }

    function normalizeSegments(segmentList) {
        var first = _.clone(segmentList[0]),
            last = segmentList[segmentList.length - 1];

        segmentList = _.clone(segmentList);

        if (belongsToSegment(first, last)) {
            first.length += last.length;
            first.pieceStart = last.pieceStart;
            segmentList[0] = first;
            segmentList.pop();
        }

        return segmentList;
    }

    function segmentizePieces() {
        var segmentList = [],
            pieceList = _.clone(pieces),
            currentSegment = createSegment(pieceList.shift(), 0);

        pieceList.forEach(function (piece, index) {
            var lengthOfPiece = getLengthOfPiece(piece, 0);

            if (!belongsToSegment(currentSegment, piece)) {
                currentSegment.pieceEnd = index;
                segmentList.push(currentSegment);
                currentSegment = createSegment(piece, index + 1);
            } else {
                currentSegment.length += lengthOfPiece;
            }

        });

        currentSegment.pieceEnd = pieces.length - 1;
        segmentList.push(currentSegment);

        return normalizeSegments(segmentList);
    }

    function getNextCurvePieceIndex(currentPosition) {
        var currentPieceIndex = currentPosition.piecePosition.pieceIndex,
            piecesToSearch = pieces.slice(currentPieceIndex + 1).concat(pieces.slice(0, currentPieceIndex)),
            firstCurveIndex;

        piecesToSearch.forEach(function (piece, index) {
            if (!isStraight(piece) && !firstCurveIndex) {
                firstCurveIndex = currentPieceIndex + index + 1;
                if (firstCurveIndex >= pieces.length) {
                    firstCurveIndex -= (piecesToSearch.length + 1);
                }
            }
        });

        return firstCurveIndex;
    }

    function getDistanceToNextCurve(currentPosition) {
        var nextCurvePieceIndex = getNextCurvePieceIndex(currentPosition),
            nextCurvePosition = {
                piecePosition: {pieceIndex: nextCurvePieceIndex, inPieceDistance: 0}
            };

        return getDistance(currentPosition, nextCurvePosition);
    }

    segments = segmentizePieces();

    return {
        isCurve: function (currentPosition) {
            var pieceIndex = currentPosition.piecePosition.pieceIndex;
            return !isStraight(pieces[pieceIndex]);
        },

        getDistanceToNextCurve: getDistanceToNextCurve,

        getSegments: function () {
            return segments;
        },

        getLengthOfPiece: getLengthOfPiece,

        getPiecesBetweenTwoPoints: getPiecesBetweenTwoPoints,

        getDistance: getDistance
    };
};
